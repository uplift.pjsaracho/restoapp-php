<!DOCTYPE html>
<html>
<head>
	<title>Home</title>

	<style type="text/css">
		* {
			box-sizing: border-box;
		}
		#items-display {
			display: flex;
			flex-wrap: wrap;			
		}
		.item-box {
			border: 1px solid black;
			padding: 15px;
			margin: 15px;
			width: calc(25% - 30px);
		}
		.item-box img {
			width: 150px;
			margin-right: 30px;
		}
		.item-details {
			display: inline-block;
			vertical-align: top;
		}
	</style>
</head>
<body>

	<h1>Home</h1>

	<div id="items-display">
		<?php 
			require_once('items.php');

			foreach($items as $item) {
				extract($item);
				echo "
					<div class='item-box'>
						<img src='$image'>
						<div class='item-details'>
							<h2>$name</h2>
							<p>Php $price</p>
						</div>
					</div>
				";
			}
		?>
	</div>
</body>
</html>
